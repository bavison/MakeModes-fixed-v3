# This file is in the form of a mode file, but it does not apply to any
# real monitor. It contains basic definitions of all the modes that are
# common to all monitor types, other than monitor type 0 (50Hz TV). The
# modes for any particular monitor can be extracted using the MakeModes
# application. Note that different kinds of monitor support other modes
# in addition to these, e.g. types 3&4 support the letterbox modes, and
# multifrequency monitors the non-letterbox equivalents. These are def-
# ined in other files.

file_format:1
monitor_title:Common Modes
DPMS_state:0

# 320 x 480 (60Hz - Modes 47, 48)
startmode
mode_name:320 x 480
x_res:320
y_res:480
pixel_rate:12587
h_timings:48,24,0,320,0,8
v_timings:2,32,0,480,0,11
sync_pol:3
endmode

# 640 x 480 (60Hz - Modes 25,26,27,28)
startmode
mode_name:640 x 480
x_res:640
y_res:480
pixel_rate:25175
h_timings:96,46,0,640,0,18
v_timings:2,32,0,480,0,11
sync_pol:3
endmode

# 640 x 480 (72Hz - VESA standard)
startmode
mode_name:640 x 480
x_res:640
y_res:480
pixel_rate:31500
h_timings:40,128,0,640,0,24
v_timings:3,28,0,480,0,9
sync_pol:3
endmode

# 640 x 480 (75Hz VESA)
startmode
mode_name:640 x 480
x_res:640
y_res:480
pixel_rate:31500
h_timings:64,112,8,640,8,8
v_timings:3,16,0,480,0,1
sync_pol:3
endmode

# 640 x 512 (50Hz - Modes 18,19,20,21)
startmode
mode_name:640 x 512
x_res:640
y_res:512
pixel_rate:24000
h_timings:56,112,0,640,0,88
v_timings:3,18,0,512,0,1
sync_pol:0
endmode

# 768 x 288 (50Hz - Modes 22,33,34,35,36)
startmode
mode_name:768 x 288
x_res:768
y_res:288
pixel_rate:16000
h_timings:76,82,0,768,0,98
v_timings:3,19,0,288,0,2
sync_pol:0
endmode

# 800 x 600 (56Hz - Modes 29,30,31 - VESA SVGA)
startmode
mode_name:800 x 600
x_res:800
y_res:600
pixel_rate:36000
h_timings:72,128,0,800,0,24
v_timings:2,22,0,600,0,1
sync_pol:0
endmode

# 800 x 600 (60Hz - VESA guideline)
startmode
mode_name:800 x 600
x_res:800
y_res:600
pixel_rate:40000
h_timings:128,88,0,800,0,40
v_timings:4,23,0,600,0,1
sync_pol:0
endmode

# 800 x 600 (72Hz - VESA standard)
startmode
mode_name:800 x 600
x_res:800
y_res:600
pixel_rate:50000
h_timings:120,64,0,800,0,56
v_timings:6,23,0,600,0,37
sync_pol:0
endmode

# 800 x 600 (75Hz VESA)
startmode
mode_name:800 x 600
x_res:800
y_res:600
pixel_rate:49500
h_timings:80,144,16,800,16,0
v_timings:3,21,0,600,0,1
sync_pol:0
endmode

# 896 x 352 (60Hz - Modes 37,38,39,40)
startmode
mode_name:896 x 352
x_res:896
y_res:352
pixel_rate:24000
h_timings:118,58,0,896,0,28
v_timings:3,9,0,352,0,0
sync_pol:2
endmode

# 1024 x 768 (60Hz - VESA guideline)
startmode
mode_name:1024 x 768
x_res:1024
y_res:768
pixel_rate:65000
h_timings:136,160,0,1024,0,24
v_timings:6,29,0,768,0,3
sync_pol:3
endmode

# 1024 x 768 (70Hz - VESA standard)
startmode
mode_name:1024 x 768
x_res:1024
y_res:768
pixel_rate:75000
h_timings:136,144,0,1024,0,24
v_timings:6,29,0,768,0,3
sync_pol:3
endmode

# 1024 x 768 (75Hz VESA)
startmode
mode_name:1024 x 768
x_res:1024
y_res:768
pixel_rate:78750
h_timings:96,96,48,1024,48,0
v_timings:3,28,0,768,0,1
sync_pol:0
endmode

# 1056 x 250 (50Hz - Modes 17)
startmode
mode_name:1056 x 250
x_res:1056
y_res:250
pixel_rate:24000
h_timings:108,72,106,1056,106,88
v_timings:3,16,20,250,20,3
sync_pol:0
endmode

# 1056 x 256 (50Hz - Modes 16,24)
startmode
mode_name:1056 x 256
x_res:1056
y_res:256
pixel_rate:24000
h_timings:108,72,106,1056,106,88
v_timings:3,16,17,256,17,3
sync_pol:0
endmode

# 1280 x 480 (60Hz pixel doubled)
startmode
mode_name:1280 x 480
x_res:1280
y_res:480
pixel_rate:50350
h_timings:192,92,0,1280,0,36
v_timings:2,32,0,480,0,11
sync_pol:3
endmode

# 1280 x 1024 (75Hz VESA)
startmode
mode_name:1280 x 1024
x_res:1280
y_res:1024
pixel_rate:135000
h_timings:144,200,32,1280,32,0
v_timings:3,38,0,1024,0,1
sync_pol:0
endmode

# 1600 x 600 (56Hz pixel doubled)
startmode
mode_name:1600 x 600
x_res:1600
y_res:600
pixel_rate:72000
h_timings:144,256,0,1600,0,48
v_timings:2,22,0,600,0,1
sync_pol:0
endmode

# 1600 x 600 (60Hz pixel doubled)
startmode
mode_name:1600 x 600
x_res:1600
y_res:600
pixel_rate:80000
h_timings:256,176,0,1600,0,80
v_timings:4,23,0,600,0,1
sync_pol:0
endmode

# 1600 x 600 (72Hz pixel doubled)
startmode
mode_name:1600 x 600
x_res:1600
y_res:600
pixel_rate:100000
h_timings:240,128,0,1600,0,112
v_timings:6,23,0,600,0,37
sync_pol:0
endmode

# End
